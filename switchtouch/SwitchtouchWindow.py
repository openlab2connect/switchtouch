# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# This file is in the public domain
### END LICENSE

from locale import gettext as _

from gi.repository import Gtk # pylint: disable=E0611
import logging
logger = logging.getLogger('switchtouch')

from switchtouch_lib import Window
from switchtouch.AboutSwitchtouchDialog import AboutSwitchtouchDialog
from switchtouch.PreferencesSwitchtouchDialog import PreferencesSwitchtouchDialog

import os
from subprocess import Popen, PIPE

# See switchtouch_lib.Window.py for more details about how this class works
class SwitchtouchWindow(Window):
    __gtype_name__ = "SwitchtouchWindow"
    
    def finish_initializing(self, builder): # pylint: disable=E1002
        """Set up the main window"""
        super(SwitchtouchWindow, self).finish_initializing(builder)

        self.AboutDialog = AboutSwitchtouchDialog
        self.PreferencesDialog = PreferencesSwitchtouchDialog

        # Code for other initialization actions should be added here.

        self.chkbtnfinger = self.builder.get_object("chkbtnfinger");
        self.chkbtnpen = self.builder.get_object("chkbtnpen");
        self.chkbtnglove = self.builder.get_object("chkbtnglove");
        self.touchstate = self.builder.get_object("touchstate");

        ''' looked for eeti touch program
        '''
        self.egloveswitch = os.path.isfile('/usr/local/bin/eGloveSwitch')
        if self.egloveswitch:
           #state = "EETI touch tool"
           #self.touchstate.set_text(state)
           
           ''' Get touch mode
           '''
           #os.system('sudo eGloveSwitch -g')
        else:
           state = "EETI touch miss tool"
           self.touchstate.set_text(state)
           return

        ''' Get touch mode via eGloveSwitch
        '''
        cmd = ['sudo', 'eGloveSwitch', '-g']
        process = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        
        output_string = stdout.decode('ascii')
        err_string = stderr.decode('ascii')

        #print 'out>\n' + output_string
        #print 'err>\n' + err_string
        
        ''' Get threshold and firmware version
        ''' 
        if "Threshold-" in output_string:
           thd_index = int(output_string.find('Threshold')) + 10
           touch_mode = output_string[thd_index]

           if touch_mode == '0':
              self.chkbtnfinger.set_active(True)
           if touch_mode == '1':
              self.chkbtnpen.set_active(True)
           if touch_mode == '2':
              self.chkbtnglove.set_active(True)

           fw_index_start = int(output_string.find('FW Version')) + 12
           fw_index_end = fw_index_start + 6
           state = "mode:" + touch_mode + " fw:" + output_string[fw_index_start: fw_index_end]
           self.touchstate.set_text(state)
        else:
           if err_string:
              self.touchstate.set_text(err_string)

    def on_chkbtnfinger_toggled(self, widget):
        mode = self.chkbtnfinger.get_active()
        if mode == False:
          on_off = "off"
        else:
          self.chkbtnpen.set_active(False)
          self.chkbtnglove.set_active(False)
          on_off = "on"

        if on_off == "on":
          
          ''' Switch finger mode ON
          '''
          cmd = ['sudo', 'eGloveSwitch', '-s', '0' ]
          process = Popen(cmd, stdout=PIPE, stderr=PIPE)
          stdout, stderr = process.communicate()

          state = "Finger mode is " + on_off
          self.touchstate.set_text(state)


    def on_chkbtnpen_toggled(self, widget):
        mode = self.chkbtnpen.get_active()
        if mode == False:
          on_off = "off"
        else:
          self.chkbtnfinger.set_active(False)
          self.chkbtnglove.set_active(False)
          on_off = "on"

        if on_off == "on":

          ''' Swithc pen mode ON
          '''
          cmd = ['sudo', 'eGloveSwitch', '-s', '1' ]
          process = Popen(cmd, stdout=PIPE, stderr=PIPE)
          stdout, stderr = process.communicate()

          state = "Pen mode is " + on_off
          self.touchstate.set_text(state)


    def on_chkbtnglove_toggled(self, widget):
        mode = self.chkbtnglove.get_active()
        if mode == False:
          on_off = "off"
        else:
          self.chkbtnfinger.set_active(False)
          self.chkbtnpen.set_active(False)
          on_off = "on"

        if on_off == "on":

          ''' Switch glove mode ON
          '''
          cmd = ['sudo', 'eGloveSwitch', '-s', '2' ]
          process = Popen(cmd, stdout=PIPE, stderr=PIPE)
          stdout, stderr = process.communicate()

          state = "Glove mode is " + on_off
          self.touchstate.set_text(state)

